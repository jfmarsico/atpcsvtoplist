//
//  AppDelegate.h
//  ATPCSVToPlist
//
//  Created by Joseph Marsico on 20/02/12.
//  Copyright (c) 2012 AppsThatPop. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
