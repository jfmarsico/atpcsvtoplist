//
//  NSArray+ATPNSArrayCVSConversion.m
//  ATPCSVToPlist
//
//  Created by Joseph Marsico on 24/02/12.
//  Copyright (c) 2012 AppsThatPop. All rights reserved.
//

#import "NSArray+ATPNSArrayCVSConversion.h"
#import "parseCSV.h"

@implementation NSArray (ATPNSArrayCVSConversion)

//
// The goal of this project is to make the equivalent
// of this line possible with a CSV file
//  NSArray *contentArray = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] 
//                                           pathForResource:@"Content" 
//                                                    ofType:@"plist"]];
//


+(NSArray *) arrayWithContentsOfCSVFile:(NSString *)aPath
{
    CSVParser *parser = [CSVParser new];
	[parser openFile:aPath];
	NSMutableArray *csvContent = [parser parseFile];
	[parser closeFile];
    
    NSMutableArray *outputArray = [NSMutableArray array];
    
	if (parser)
	{
        
		NSArray *keyArray = [csvContent objectAtIndex:0];
        
		NSInteger i = 0;
		for (NSArray *array in csvContent)
		{
			NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
            
			NSInteger keyNumber = 0;
            
			for (NSString *string in array)
			{
                
				[dictionary setObject:string forKey:[keyArray objectAtIndex:keyNumber]];
                
				keyNumber++;
                
			}
            
			if (i > 0)
			{
				[outputArray addObject:dictionary];
			}
            
			i++;
		}
        
	}
    return outputArray;
}

@end
