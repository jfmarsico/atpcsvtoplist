//
//  main.m
//  ATPCSVToPlist
//
//  Created by Joseph Marsico on 20/02/12.
//  Copyright (c) 2012 AppsThatPop. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
