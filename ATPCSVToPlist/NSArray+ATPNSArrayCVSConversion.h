//
//  NSArray+ATPNSArrayCVSConversion.h
//  ATPCSVToPlist
//
//  Created by Joseph Marsico on 24/02/12.
//  Copyright (c) 2012 AppsThatPop. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSArray (ATPNSArrayCVSConversion)

+(NSArray *) arrayWithContentsOfCSVFile:(NSString *)aPath;

@end
